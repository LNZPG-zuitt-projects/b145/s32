/* Mini-Activity
Initialize npm in discussion folder
Install express, mongoose and nodemon modules
Start the nodemon under scripts in package.json file
Create a .gitignore file and add the node_modules

In your index.js- create simple express server 

ENDS in 15 MINUTES- 6:01PM
*/

// Express server

const express = require('express');
const mongoose = require('mongoose');
const port = 4000;
const app = express();


// Connecting MongoDB
mongoose.connect("mongodb+srv://NeilGopez:Claudia143@b145.daxag.mongodb.net/session32?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"));
db.once("open", () => console.log("Successfully connected to the database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Mongoose Schemas
	// schema ===  blueprint of your data

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: 'pending'
	}
});

// Model
const Task = mongoose.model("Task", taskSchema);

// Business Logic

// Creating a new task
app.post("/tasks", (req, res) => {

	Task.findOne({name : req.body.name}, (err, result) => {

		if(result != null && result.name === req.body.name){

			return res.send(`Duplicate task found: ${err}`)

		} else {

			let newTask = new Task({
				name : req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(200).send(`New task created : ${savedTask}`)
				};
			});
		};
	});
});

// Retrieving all tasks
app.get("/tasks", (req, res) => {

	Task.find({}, (err, result) => {

		if(err){
			
			return console.log(err);
		
		} else {

			return res.status(200).json({
				tasks : result
			})
		}
	})
})

// Updating Task Name
app.put("/tasks/update/:taskId", (req, res) => {

	let taskId = req.params.taskId
	let name = req.body.name

	Task.findByIdAndUpdate(taskId, {name: name}, (err, updatedTask) => {
		if(err){
			console.log(err)
		} else {
			res.send(`Congratulations the task has been updated`);
		}
	})
})

// Delete Task
app.delete("/tasks/archive-task/:taskId", (req, res) => {

	let taskId = req.params.taskId;

	Task.findByIdAndDelete(taskId, (err, deletedTask) => {
		if(err){
			console.log(err)
		} else {
			res.send(`${deletedTask} has been deleted`)
		}
	})
})


// THIS IS THE ACTIVITY PART
// 1. Create a User schema.
// 	<email>: String,	
// 	<username> : String,
// 	<password> : String,
// 	<age> : Number
// 	<isAdmin> : Boolean, default: false


// 2. Create a User model.

const Task = mongoose.model("Task", actSchema);
const userSchema = new mongoose.Schema({

	email: String,	
	username : String,
	password : String,
	age : Number,
	isAdmin : {
		type : Boolean,
		default: false
	}
});

const User = mongoose.model("User", userSchema);
// 3. Create a POST route to register a user.

app.post("/users/signup", (req, res) => {

// 4. Name the endpoint /users/signup
// 	Business Logic:
// 		Add a functionality to check if there are duplicate users
// 		- If the email already exists in the database, we return an error
// 		- If the email doesn't exist in the database, we add the in the database
// 		The user data will be coming from the request's body
// 		Create a new User object with a “email, "username", "password" and “age” fields/properties

	User.findOne({email : req.body.email}, (err, result) => {

		if(result != null && result.email == req.body.email){

			return res.send(`User already exists: ${err}`)

		} else {

			let newUser = new User({
				email: req.body.email,	
				username : req.body.username,
				password : req.body.password,
				age : req.body.age
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(200).send(`New user registered: ${savedUser}`)
				};
			});
		};
	});
});



// 5. Process a POST request at the /users/signup route using postman to register a user.

// 6. Retrieve all users that registered using the endpoint /users.


app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else{
			return res.status(200).json({
				users : result
			})
		}
	})
});

// 7. Process a GET request at the /users in postman to retrieve all users.

app.put('/users/update-user/:userId', (req, res) => {
	let userId = req.params.userId

	let username = req.body.username

	User.findByIdAndUpdate(userId, {username: username}, (err, updatedUser) => {
		if(err){
			console.log(err);
		} else  {
			res.send(`Conratulations, ${updatedUser} has been updated!`)
		}
	})
});

// 8. Create an endpoint that will be able to update the user’s username. Name the endpoint /users/update-user/:wildcard

// 9. Process a PUT request in postman to update a user.

// 10. Create an endpoint that can delete a user. Name the endpoint /users/archive-user/:wildcard

// 11. Process a DELETE request in postman to delete a user.

app.delete(`/users/archive-user/:userId`, (req, res) => {

	let userId = req.params.userId;

	User.findByIdAndDelete(userId, (err, deletedUser) => {
		if(err){
			console.log(error);
		} else {
			res.send(`User ${deletedUser.username} archived!`)
		}
	});
});

// 12. Create an image folder containing all your postman outputs.

// 13. Export your postman collection






app.listen(port, () => console.log(`Server running at port ${port}`))
